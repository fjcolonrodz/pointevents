<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    $events = \App\Event::all();
//    return view('public.index', compact('events'));
//})->name('home');

Route::get('/', function () {
    return redirect()->route('events.show',1);
});

Route::get('/events/{event}', 'EventsController@show')->name('events.show');
Route::post('/registration', 'RegistrationController@storeRunner')->name('registration.store');
Route::get('/sendMail/{id}', 'RegistrationController@notifyRunner')->name('registration.mail');

Auth::routes(['register' => false]);

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {
    Route::get('/', function() {
        $events = \App\Event::all();
        return view('admin.index', compact('events'));
    })->name('dashboard');

    Route::get('events', 'EventsController@index')->name('events.index');
    Route::get('events/{event}/runners', 'EventsController@runnersIndex')->name('events.runners.index');
    Route::get('events/{event}/runners/export', 'EventsController@runnersExport')->name('events.runners.export');
    Route::get('events/{event}/runners/export_all', 'EventsController@runnersExportAll')->name('events.runners.export_all');
    Route::get('events/{event}/runners/export', 'EventsController@runnersExport')->name('events.runners.export');
    Route::get('events/{event}/runners/pending', 'EventsController@runnersPending')->name('events.runners.pending');
    Route::get('events/{event}/runners/create', 'EventsController@runnersCreate')->name('events.runners.create');
    Route::post('events/{event}/runners/create', 'EventsController@runnersStore')->name('events.runners.store');
    Route::get('events/{event}/runners/{runner}', 'EventsController@runnersEdit')->name('events.runners.edit');
    Route::put('events/{event}/runners/{runner}', 'EventsController@runnersUpdate')->name('events.runners.update');
    Route::delete('events/{event}/runners/{runner}', 'EventsController@runnersDestroy')->name('events.runners.destroy');
    Route::get('events/{event}/runners/{runner}/confirm', 'EventsController@runnersConfirm')->name('events.runners.confirm');


//    Route::get('runners/export', 'RunnerController@exportCSV')->name('runners.export');
//    Route::get('runners/print/confirmed', 'RunnerController@printConfirmed')->name('runners.printConfirmed');
//    Route::get('runners/print/unconfirmed', 'RunnerController@printUnconfirmed')->name('runners.printUnconfirmed');
});
