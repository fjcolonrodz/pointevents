<?php

namespace App\Jobs;

use App\Mail\RegistrationConfirmation;
use App\Mail\RelativeRegistrationConfirmation;
use App\Mail\YoungRegistrationConfirmation;
use App\Runner;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendRegistrationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $runner;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Runner $runner)
    {
        $this->runner = $runner;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $to = array(
            [
                'email' => $this->runner->email,
                'name' => $this->runner->full_name,
            ]
        );

        if ($this->runner->relative_name == null) {
            Mail::to($to)->send(new RegistrationConfirmation($this->runner));
        } else {
            Mail::to($to)->send(new YoungRegistrationConfirmation($this->runner));
            Mail::to($to)->send(new RelativeRegistrationConfirmation($this->runner));
        }
    }
}
