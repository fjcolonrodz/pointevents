<?php

namespace App\Mail;

use App\Runner;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelativeRegistrationConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    protected $runner;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Runner $runner)
    {
        $this->runner = $runner;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $runner = $this->runner;
        return $this->markdown('emails.runner.relative', compact('runner'))
                    ->to($runner->relative_email, $runner->relative_name)
                    ->bcc('maratonlacandelaria@gmail.com', 'Carrera 10k La Candelaria - Equipo de Inscripciones')
                    ->subject('Solicitud de Inscripción: ' . $runner->full_name);
    }
}
