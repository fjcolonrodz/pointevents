<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    protected $dates = ['date'];

    protected $dateFormat = 'Y-m-d';

    protected $guarded = [];

    function event_types()
    {
        return $this->belongsToMany(EventType::class);
    }

    function shirts()
    {
        return $this->hasMany(Shirt::class);
    }

    function races()
    {
        return $this->hasMany(Race::class);
    }

    public function runners()
    {
        return $this->hasManyThrough(Runner::class, Race::class);
    }

    function user()
    {
        return $this->belongsTo(User::class);
    }
}
