<?php

namespace App\Http\Controllers;

use App\Jobs\SendRegistrationEmail;
use App\Mail\RegistrationConfirmation;
use App\Mail\RelativeRegistrationConfirmation;
use App\Mail\YoungRegistrationConfirmation;
use App\Runner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegistrationController extends Controller
{

    function storeRunner(Request $request)
    {
        if ($request['shirt_id'] == null) {
            $request['shirt_id'] = 9;
        }

        $created_runner = Runner::create($request->all());

        if ($created_runner) {
            $created_runner->confirmed = 1;
            $created_runner->save();
        } else {
            $request->session()->flash('error');
        }

        return response()->json(['success' => 'El corredor ha sido creado exitosamente', 'runner' => $created_runner]);
    }

    function notifyRunner($id)
    {
        $runner = Runner::findOrFail($id);

        SendRegistrationEmail::dispatch($runner)->onQueue('emails');

        session(['message' => 'El correo ha sido enviado exitosamente']);

        return response()->json(['success' => 'El corredor ha sido creado exitosamente']);
    }

}
