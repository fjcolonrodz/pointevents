<?php

namespace App\Http\Controllers;

use App\City;
use App\Event;
use App\Http\Requests\StoreRegistration;
use App\Jobs\SendRegistrationEmail;
use App\Race;
use App\Runner;
use App\Shirt;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    function index()
    {
        $events = Event::all();
        return view('admin.events.index', compact('events'));
    }
    function show($id)
    {
        $event = \App\Event::findOrFail($id);
        $cities = \App\City::all();
        return view('public.show', compact('event', 'cities'));
    }

    /**
     *
     */

    function runnersIndex($id)
    {
        $event = Event::findOrFail($id);
        return view('admin.events.runners', compact('event'));
    }
    function runnersCreate(Event $event)
    {
        $races = Race::select('id', 'description')->get();
        $cities = City::select('id', 'description')->get();
        $shirts = Shirt::all();

        return view('admin.runners.create', compact('event', 'races', 'cities', 'shirts'));
    }
    function runnersStore(Event $event, StoreRegistration $registration)
    {
        $created_runner = Runner::create($registration->all());

        SendRegistrationEmail::dispatch($created_runner)->onQueue('emails');

        return redirect()->route('events.runners.index', compact('event'));
    }
    function runnersEdit(Event $event, Runner $runner)
    {
        $races = Race::select('id', 'description')->get();
        $cities = City::select('id', 'description')->get();
        $shirts = Shirt::select('id', 'size')->get();

        return view('admin.runners.edit', compact('event','runner', 'races', 'cities', 'shirts'));
    }
    function runnersUpdate(Event $event, Runner $runner, Request $request)
    {
        $runner->update($request->all());
        return redirect()->route('events.runners.index', compact('event'));
    }
    function runnersDestroy(Event $event, Runner $runner)
    {
        $runner->delete();
        return redirect()->route('events.runners.index', compact('event'));
    }

    function runnersPending(Event $event)
    {
        $runners = Runner::unconfirmed();

        return view('admin.runners.pending', compact('event','runners'));
    }
    public function runnersConfirm(Event $event, Runner $runner)
    {
        $runner->confirmed = 1;
        $runner->save();

        return redirect()->route('events.runners.index', compact('event'));
    }
    public function runnersExport()
    {

        $runners = Runner::confirmed();
        $csvExporter = new \Laracsv\Export();

        $csvExporter->beforeEach(function ($runner) {
            $birth_date = new \DateTime($runner->birth_date);
            $now = new \DateTime();
            $interval = $now->diff($birth_date);
            $runner->birth_date = $interval->y;

            $runner->gender == 0 ? $runner->gender = 'M' : $runner->gender = 'F';
        });

        $csvExporter->build($runners, [
            'name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'gender' => 'Gender',
            'birth_date' => 'Age',
        ])->download('5kCandelCoop2020.csv');

    }
    public function runnersExportAll()
    {
        $runners = Runner::confirmed();
        $csvExporter = new \Laracsv\Export();

        $csvExporter->beforeEach(function ($runner) {
            $runner->name = $runner->full_name;

            $address = '';
            if ($runner->street_2 !== '') {
                $address = $runner->street_1 . ' ' . $runner->street_2;
            } else {
                $address = $runner->street_1;
            }
            $runner->address_1 = $address . ' ' . $runner->city->description . ', ' . $runner->country . ' ' . $runner->zipcode->description;


        });

        $csvExporter->build($runners, [
            'name' => 'Full Name',
            'address_1' => 'Address',
            'phone' => 'Phone'
        ])->download('5kCandelCoop2020_AllRunners.csv');

    }
}
