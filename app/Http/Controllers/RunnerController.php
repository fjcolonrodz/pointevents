<?php

namespace App\Http\Controllers;

use App\City;
use App\Race;
use App\Shirt;
use App\Runner;
use App\RaceType;
use App\Http\Requests\StoreRegistration;
use Illuminate\Http\Request;

class RunnerController extends Controller
{

    public function index()
    {
        $runners = Runner::all();

        return view('admin.runners.index', compact('runners'));
    }

    function create()
    {
        $races = Race::select('id', 'description')->get();
        $cities = City::select('id', 'description')->get();
        $shirts = Shirt::all();

        return view('admin.runners.create', compact('races', 'cities', 'shirts'));
    }



    function edit(Runner $runner)
    {
        $races = Race::select('id', 'description')->get();
        $cities = City::select('id', 'description')->get();
        $shirts = Shirt::select('id', 'size')->get();

        return view('admin.runners.edit', compact('runner', 'races', 'cities', 'shirts'));
    }

    function update(Runner $runner, Request $request)
    {
        $runner->update($request->all());
        return redirect()->route('runners.index');
    }

    function destroy(Runner $runner)
    {
        $runner->delete();
        return redirect()->route('runners.index');
    }

    public function exportCSV()
    {

        $runners = Runner::confirmed();
        $csvExporter = new \Laracsv\Export();

        $csvExporter->beforeEach(function ($runner) {
            $birth_date = new \DateTime($runner->birth_date);
            $now = new \DateTime();
            $interval = $now->diff($birth_date);
            $runner->birth_date = $interval->y;

            if ($runner->race_id == 2) {
                $runner->race_id = "Caminante";
            } elseif ($runner->race_id == 3) {
                $runner->race_id = "5k";
            } elseif ($runner->race_id == 4) {
                $runner->race_id = "Silla de Ruedas";
            }

//            if ($runner->race_type_id == 1 && $runner->gender == 0 && $runner->runner_type == 1) {
//                $runner->race_type_id = "E";
//            } elseif ($runner->race_type_id == 1 && $runner->gender == 1 && $runner->runner_type == 1) {
//                $runner->race_type_id = "F";
//            } elseif ($runner->race_type_id == 1 && $runner->gender == 0 && $runner->runner_type == 2) {
//                $runner->race_type_id = "A";
//            } elseif ($runner->race_type_id == 1 && $runner->gender == 1 && $runner->runner_type == 2) {
//                $runner->race_type_id = "B";
//            } elseif ($runner->race_type_id == 1 && $runner->gender == 0 && $runner->runner_type == 3) {
//                $runner->race_type_id = "C";
//            } elseif ($runner->race_type_id == 1 && $runner->gender == 1 && $runner->runner_type == 3) {
//                $runner->race_type_id = "D";
//            }

            $runner->gender == 0 ? $runner->gender = 'M' : $runner->gender = 'F';
            $runner->sport_type = 'Running';
        });

        $csvExporter->build($runners, [
            'id' => "Bib",
            'name' => 'First Name',
            'last_name' => 'Last Name',
            'gender' => 'Gender',
            'birth_date' => 'Age',
            'race_id' => 'Race',
            'sport_type' => 'Sport type'
        ])->download('5kcandelcoop2019.csv');

    }

    public function confirmed()
    {
        $runners = Runner::confirmed();

        return view('admin.runners.confirmed', compact('runners'));
    }

    public function pending()
    {
        $runners = Runner::unconfirmed();

        return view('admin.runners.pending', compact('runners'));
    }

    public function confirmRunner($id)
    {
        $runner = Runner::findOrFail($id);

        $runner->confirmed = 1;

        $runner->save();

//        Mail::send(new PaymentReceived($runner));

        return redirect()->route('runners.index');
    }

    public function unconfirmRunner($id)
    {
        $runner = Runner::findOrFail($id);

        $runner->confirmed = 0;

        $runner->save();

        return redirect()->route('runners.index');
    }

    public function printConfirmed()
    {
        $report_title = "Corredores Online Confirmados";
        $runners = Runner::confirmed();

        foreach ($runners as $runner) {
            $birth_date = new \DateTime($runner->birth_date);
            $now = new \DateTime();
            $interval = $now->diff($birth_date);
            $runner->birth_date = $interval->y;
        }

        $pdf = \PDF::loadView('admin.reports.confirmed', compact('report_title', 'runners'));
        return $pdf->setPaper('letter', 'landscape')->stream('corredores_confirmados_2017.pdf');
    }

    public function printUnconfirmed()
    {
        $report_title = "Corredores Online Sin Confirmar";
        $runners = Runner::unconfirmed();

        foreach ($runners as $runner) {
            $birth_date = new \DateTime($runner->birth_date);
            $now = new \DateTime();
            $interval = $now->diff($birth_date);
            $runner->birth_date = $interval->y;
        }

        $pdf = \PDF::loadView('admin.reports.confirmed', compact('report_title', 'runners'));
        return $pdf->setPaper('letter', 'landscape')->stream('corredores_confirmados_2017.pdf');
    }

}
