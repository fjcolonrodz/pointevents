<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRegistration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'race_id' => 'required',
            'name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'gender' => 'required',
            'street_1' => 'required',
            'city_id' => 'required',
            'country' => 'required',
            'zipcode_id' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'emergency_contact' => 'required',
            'emergency_contact_phone' => 'required',
            'payment_method' => 'required',
            'shirt_id' => 'required',
        ];
    }
}
