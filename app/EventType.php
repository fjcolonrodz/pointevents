<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    protected $guarded = [];

    function events()
    {
        return $this->belongsToMany(Event::class);
    }
}