<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Runner extends Model
{
    use SoftDeletes;

    const BIB_NUMBER = null;
    const CONFIRMATION_CODE = null;
    const TERMS_ACCEPTANCE = 1;

    protected $attributes = [
        'bib_number' => self::BIB_NUMBER,
        'confirmation_code' => self::CONFIRMATION_CODE,
        'terms_acceptance' => self::TERMS_ACCEPTANCE,
    ];

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->generateBibNumber(1, 400);
            $model->generateConfirmationCode(15);
        });
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function events()
    {
        return $this->hasManyThrough(Event::class, Race::class);
    }

    public function race()
    {
        return $this->belongsTo(Race::class);
    }

    public function shirt()
    {
        return $this->belongsTo(Shirt::class);
    }

    public function zipcode()
    {
        return $this->belongsTo(Zipcode::class, 'zipcode_id');
    }

    /*
     *
     */
    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->last_name;
    }

    public function scopeCount5k($query)
    {
        return $query->where('race_type_id', 1)->count();
    }

    public function scopeCount10k($query)
    {
        return $query->where('race_type_id', 2)->count();
    }

    public function scopeConfirmedCount($query)
    {
        return $query->where('confirmed', 1)->count();
    }

    public function scopeConfirmed($query)
    {
//        return $query->where('confirmed', 1)->where('race_id', '!=', 1)->orderBy('name')->get();
        return $query->where('confirmed', 1)->get();
    }

    public function scopeUnconfirmed($query)
    {
        return $query->where('confirmed', 0)->orderBy('name')->get();
    }

    /*
     * Propietary functions
     */
    protected function generateBibNumber($min, $max)
    {
        do {
            $bib = mt_rand($min, $max);
        } while (!empty(Runner::where('bib_number', $bib)->first()));

        if (is_null($this->attributes['bib_number']))
        {
            $this->attributes['bib_number'] = $bib;
        }
    }

    protected function generateConfirmationCode($length) {
        $randstr = "";
        srand((double) microtime(TRUE) * 1000000);
        //our array add all letters and numbers if you wish
        $chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
            'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        for ($rand = 0; $rand <= $length; $rand++) {
            $random = rand(0, count($chars) - 1);
            $randstr .= $chars[$random];
        }

        if (is_null($this->attributes['confirmation_code']))
        {
            $this->attributes['confirmation_code'] = $randstr;
        }
    }
}
