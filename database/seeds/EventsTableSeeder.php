<?php

use Illuminate\Database\Seeder;

use App\Event;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = [
            [
                'user_id' => 2,
                'phone' => '(787) 457-4887',
                'email' => 'asaproductionincpr@gmail.com',
                'logo' => 'images/logos/logo_5k.jpg',
                'website' => 'https://www.candelcoop.coop/',
                'description' => '<p>Vuelve la 5ta edición de Arranca Pal\' 5K de Candel Coop.</p>
                            <p>Una actividad para el disfrute familiar. A celebrarse este año de manera virtual.
                               Camínalo, córrelo, comparte el evento con tús amistades y familia.  Un evento que no te
                               puedes perder y siguiendo todos los Protocolos de distanciamiento social establecidos
                               por la Orden Ejecutiva.</p>
                            <p>Se premiará con $300 (1er lugar Overall) $200 (2do lugar Overall) $100 (3er lugar Overall).</p>
                            <p>También tendremos premiaciones en metálico para los 1eros, 2ndos, 3eros lugares de cada categoría ($50, $30, $20).</p>
                            <p>Medallas para las carreras de niños, camisas en microfibra alusivas al evento para las
                               primeras 250 Inscripciones y medallas para las primeras 250 personas en llegar a la meta.</p>
                            <p>¡Aprovecha y riega la voz e inscribete ya!</p>',
                'name' => 'Arranca pal 5k de Candel Coop - Virtual',
                'date' => '2020-11-21',
                'price' => 22.00,
                'agreement' => 'Aquí van los agreements'
            ]
        ];

        foreach ($events as $event)
        {
            Event::create($event);
        }
    }
}
