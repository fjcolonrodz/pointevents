<?php

use Illuminate\Database\Seeder;

use App\Shirt;

class ShirtsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shirts = [
            ['event_id' => 1, 'size' => '6-8', 'quantity' => 0],
            ['event_id' => 1, 'size' => '10-12', 'quantity' => 0],
            ['event_id' => 1, 'size' => '14-16', 'quantity' => 0],
            ['event_id' => 1, 'size' => 'Small', 'quantity' => 0],
            ['event_id' => 1, 'size' => 'Medium', 'quantity' => 0],
            ['event_id' => 1, 'size' => 'Large', 'quantity' => 0],
            ['event_id' => 1, 'size' => 'X-Large', 'quantity' => 0],
            ['event_id' => 1, 'size' => 'XX-Large', 'quantity' => 0],
            ['event_id' => 1, 'size' => 'N/A', 'quantity' => 0],
        ];

        foreach ($shirts as $shirt)
        {
            Shirt::create($shirt);
        }
    }
}
