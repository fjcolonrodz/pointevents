<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RolesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(EventTypesTableSeeder::class);
         $this->call(EventsTableSeeder::class);
         $this->call(ShirtsTableSeeder::class);
         $this->call(RacesTableSeeder::class);
         $this->call(CitiesTableSeeder::class);
         $this->call(ZipCodesTableSeeder::class);
    }
}
