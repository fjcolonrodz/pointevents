<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'role_id' => 1,
                'name' => 'Francisco J. Colón-Rodríguez',
                'email' => 'fjcr1018@gmail.com',
                'password' => Hash::make('5kCandelCoop2020'),
            ],
            [
                'role_id' => 2,
                'name' => 'Sharlanne Reyes',
                'email' => 'asaproductionincpr@gmail.com',
                'password' => Hash::make('5kCandelCoop2020'),
            ]
        ];

        foreach ($users as $user)
        {
            User::create($user);
        }
    }
}
