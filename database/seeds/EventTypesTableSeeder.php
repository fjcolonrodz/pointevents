<?php

use Illuminate\Database\Seeder;

use App\EventType;

class EventTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $event_types = [
            ['description' => '5k'],
            ['description' => '10k'],
            ['description' => 'Dúalo'],
            ['description' => 'Tríalo'],
            ['description' => 'Challenge'],
            ['description' => 'Mountain Bike'],
        ];

        foreach ($event_types as $event_type)
        {
            EventType::create($event_type);
        }
    }
}
