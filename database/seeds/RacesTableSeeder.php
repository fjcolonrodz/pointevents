<?php

use Illuminate\Database\Seeder;

use App\Race;

class RacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $races = [
            ['event_id' => 1, 'description' => 'Carrera de Niños'],
            ['event_id' => 1, 'description' => 'Carrera 5k - Caminante'],
            ['event_id' => 1, 'description' => 'Carrera 5k - Corredor'],
            ['event_id' => 1, 'description' => 'Carrera 5k - Silla de Ruedas'],
        ];

        foreach ($races as $race)
        {
            Race::create($race);
        }
    }
}
