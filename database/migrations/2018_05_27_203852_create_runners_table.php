<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRunnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('runners', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('confirmed')->default(false);
            $table->string('confirmation_code');
            $table->integer('race_id')->unsigned();
            $table->integer('bib_number');
            $table->string('name');
            $table->string('initial')->nullable();
            $table->string('last_name');
            $table->string('birth_date');
            $table->boolean('gender');
            $table->string('street_1');
            $table->string('street_2')->nullable();
            $table->integer('city_id')->unsigned();
            $table->string('country');
            $table->integer('zipcode_id')->unsigned();
            $table->string('phone');
            $table->string('email');
            $table->string('group_name')->nullable();
            $table->string('emergency_contact');
            $table->string('emergency_contact_phone');
            $table->integer('shirt_id')->unsigned();
            $table->boolean('terms_acceptance');
            $table->string('payment_method');
            $table->string('relative_name')->nullable();
            $table->string('relative_email')->nullable();
            $table->string('relative_phone')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('race_id')->references('id')->on('races');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('zipcode_id')->references('id')->on('zipcodes');
            $table->foreign('shirt_id')->references('id')->on('shirts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('runners');
    }
}
