{{--<script src="https://js.braintreegateway.com/web/3.25.0/js/client.min.js"></script>--}}
{{--<script src="https://js.braintreegateway.com/web/3.25.0/js/paypal-checkout.min.js"></script>--}}
<script type="text/javascript" src="https://www.paypalobjects.com/api/checkout.js"></script>

<script>

    paypal.Button.render({
        env: '{{ env('PAYPAL_ENV') }}', // 'production' Or 'sandbox',

        client: {
            sandbox:    'ARDqzukx6W3PLbATbXU_NKNT_9WY4OXncnOv8PML4UCnwrW6-9B0A1w8DLft7YtOPCIcU35PEV4WptvU',
            production: 'AZhRMwaxowR0aFHON8OgEsZVqX-B_CNmJEwUs9LCAdjydlvOq_6Y84dsEXdplIQA1wihruS1_bPMpUOQ',

            sandbox:    '{{ env('PAYPAL_SANDBOX_KEY') }}',
            production: '{{ env('PAYPAL_PROD_KEY') }}'
        },

        commit: true, // Show a 'Pay Now' button

        style: {
            layout: 'vertical',  // horizontal | vertical
            size:   'responsive',    // medium | large | responsive
            shape:  'rect',      // pill | rect
            color:  'gold'       // gold | blue | silver | black
        },

        funding: {
            allowed: [ paypal.FUNDING.CARD, paypal.FUNDING.CREDIT ],
            disallowed: [ ]
        },

        payment: function(data, actions) {
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: '22.00', currency: 'USD' }
                        }
                    ]
                }
            });
        },

        onAuthorize: function(data, actions) {
            return actions.payment.execute().then(function(payment) {
                // The payment is complete!
                // You can now show a confirmation message to the customer
                $('#payment').modal('hide');
                waitingDialog.show('Estamos procesando tu inscripción. \<br>¡Solo tomará unos segundos! ;)', {
                    headerText: 'Procesando...',
                    dialogSize: 'md',
                    progressType: 'success'
                });

                let data = $('#registration_form').serializeArray()
                data.push({name: 'payment_method', value: 'PayPal'})

                $.post('/registration', data, function (response) {
                    console.log(response);
                    if (response.success) {
                        waitingDialog.hide();
                        $.get('/sendMail/' + response.runner.id, function (response) {
                            if (response.success) {
                                waitingDialog.hide();
                                window.location = '{{ route('events.show', 1) }}';
                            }
                        })
                    }
                })
            });
        },

        onCancel: function(data, actions) {
            /*
             * Buyer cancelled the payment
             */
            console.log('checkout.js payment cancelled', JSON.stringify(data, 0, 2));
        },

        onError: function(err) {
            /*
             * An error occurred during the transaction
             */
            console.error('checkout.js error', err);
        }
    }, '#paypal-button');

</script>

<script>
    ATHM_Checkout = {

        env: '{{ env('ATH_MOVIL_ENV') }}',
        publicToken: '{{ env('ATH_MOVIL_KEY') }}',

        timeout: 180,

        theme: 'btn',
        lang: 'es',

        total: 22.00,
        tax: 0.00,
        subtotal: 22.00,

        onCompletedPayment: function(response) {
            $('#payment').modal('hide');
            waitingDialog.show('Estamos procesando tu inscripción. \<br>¡Solo tomará unos segundos! ;)', {
                headerText: 'Procesando...',
                dialogSize: 'md',
                progressType: 'success'
            });

            let data = $('#registration_form').serializeArray()
            data.push({name: 'payment_method', value: 'ATH Móvil'})

            $.post('/registration', data, function (response) {
                console.log(response);
                if (response.success) {
                    waitingDialog.hide();
                    $.get('/sendMail/' + response.runner.id, function (response) {
                        if (response.success) {
                            waitingDialog.hide();
                            window.location = '{{ route('events.show', 1) }}';
                        }
                    })
                }
            })
        },
        onCancelledPayment: function(response) {
            console.log('ATH Móvil payment cancelled');
        },
        onExpiredPayment: function(response) {
            console.log('ATH Móvil Expired');
        }
    };
</script>
<script src="https://www.athmovil.com/api/js/v2/athmovilV2.js"></script>
