<script>

    // $("#race_id").change(function() {
    //     var event = $("#race_id").val();
    //     if (event === '1') {
    //         $('#shirt_id').hide();
    //         $('#adult').hide();
    //         $('#child').show();
    //     } else {
    //         $('#shirt_id').show();
    //         $('#adult').show();
    //         $('#child').hide();
    //     }
    // });

    $('#child').hide();

    $(".date").change(function(){

        var today = new Date();
        var birthDate = new Date($('.date').val());
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }

        if (age < 18) {
            $('#authorization').show();
        } else {
            $('#authorization').hide();
        }

        return $('#age').html(age+' years old');
    });
</script>
