<script>
    function validateForm()
    {
        let validator = $( "#registration_form" ).validate({
            errorClass: 'is-invalid'
        });
        validator.form();

        if (validator.form()) {
            $('#terms_acceptance').modal('show');
        }
    }

    function showPayment()
    {
        $('#register').modal('hide');
        $('#payment_options').show();
    }

    function processKidRegistration() {
        let data = $('#registration_form').serializeArray()
        data.push({name: 'payment_method', value: 'FREE'})

        $.post('/registration', data, function (response) {
            console.log(response);
            if (response.success) {
                waitingDialog.hide();
                $.get('/sendMail/' + response.runner.id, function (response) {
                    if (response.success) {
                        waitingDialog.hide();
                        window.location = '{{ route('events.show', 1) }}';
                    }
                })
            }
        })
    }
</script>
