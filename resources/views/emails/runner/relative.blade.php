@component('mail::message')
<img src="{{ asset($runner->race()->first()->event->logo) }}" alt="" height="100"><br><br>

# Confirmación de Inscripción

Hemos recibido una solicitud por parte de **{{ $runner->full_name }}** para participar de {{ $runner->race()->first()->event->name }}.
¡{{ $runner->relative_name }}, esta es la confirmación de la inscripción de su hijo/a!

## Datos Registrados

**Número de Confirmación:** {{ $runner->confirmation_code }} <br>
**Evento Registrado:** {{ $runner->race()->first()->description }} <br>
**Nombre:** {{ $runner->full_name }} <br>
**Dirección:** {{ $runner->street_1 . ' ' . $runner->city()->first()->description . ', ' . $runner->country }} <br>
**Teléfono:** {{ $runner->phone }} <br>
**Género:** {{ $runner->gender == "0" ? 'Masculino' : 'Femenino' }} <br>
**Tamaño de T-Shirt:** {{ $runner->shirt()->first()->size }}

## Información importante
- En los próximos días recibirás un correo electrónico confirmando tu registro a la plataforma Active. Esta es la plataforma que estaremos utilizando para llevar a cabo el cronometraje del evento virtual
- Dicho correo electrónico contiene un "Registration ID". Es importante que tengas este número a la mano al momento de somenter tus resultados finales a la plataforma.
- Los resultados deben ser sometidos al siguiente enlace: <a href="https://resultscui.active.com/events/Arranca5kCandelCoopVirualRace">https://resultscui.active.com/events/Arranca5kCandelCoopVirualRace</a>

{{--Documentos importantes:--}}
{{--- <a href="{{ asset('reglamentos/' . $runner->race()->first()->event->id . '.pdf') }}">Reglamento de la  carrera</a>--}}
{{--- <a href="{{ asset('mapas/' . $runner->race()->first()->event->id . '.jpg') }}">Mapa de la ruta</a>--}}

De haber algun error en la información registrada, favor notificarlo a <a href="mailto:{{ $runner->race()->first()->event->email }}">{{ $runner->race()->first()->event->email }}</a>

¡Nos vemos en la carrera!

Gracias,<br>
Equipo de Inscripciones
@endcomponent

