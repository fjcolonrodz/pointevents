@component('mail::message')
<img src="{{ asset($runner->race()->first()->event->logo) }}" alt="" height="100"><br><br>

# En sus marcas...listos...

¡{{ $runner->full_name }}, ya estamos casi listos con tu inscripción!

Antes de seguir con el proceso de inscripción necesitamos confirmar que estas autorizado por papá, mamá o alguna persona
encargada. Ya le enviamos un correo electrónico a **{{ $runner->relative_email }}**. ¡Asegúrate que lo vean y estaremos listos!
Una vez confirmada la autorización te enviaremos todos los detalles de la carrera a tu correo electrónico.

¡Nos vemos en la carrera!

Gracias,<br>
Equipo de Inscripciones
@endcomponent
