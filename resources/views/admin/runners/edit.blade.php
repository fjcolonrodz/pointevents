@extends('admin.layout.main')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <div class="row">
                    <div class="col-sm-9">
                        <h2>Actualizar corredor</h2>
                    </div>
                    <div class="col-sm-3">
                        <form action="{{ route('events.runners.destroy', ['event' => $event, 'runner' => $runner]) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="delete">
                            <button class="btn btn-block btn-sm btn-danger" onclick="return confirm('Está seguro que desea eliminar el corredor?')">Eliminar corredor</button>
                        </form>
                    </div>
                </div>
            </div>

            <hr>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('events.runners.update', ['event' => $event, 'runner' => $runner]) }}" method="post" id="registration_form">

                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">

                <label for="race_id">Tipo de Carrera</label><br>
                <select class="custom-select" name="race_id">
                    <option value="">Seleccione el Tipo de Carrera</option>
                    @foreach($races as $race)
                        <option value="{{ $race->id }}" {{ old('race_id', $runner->race_id) == $race->id ? 'selected' : '' }}>{{ $race->description }}</option>
                    @endforeach
                </select>

                <br><br>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">BIB Number</label>
                            <input class="form-control" name="id" value="{{ old('name', $runner->id) }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input class="form-control" name="name" value="{{ old('name', $runner->name) }}">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="name">Inicial</label>
                            <input class="form-control" name="initial" value="{{ old('initial', $runner->initial) }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="name">Apellidos</label>
                            <input class="form-control" name="last_name" value="{{ old('last_name', $runner->last_name) }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="birth_date">Fecha de Nacimiento</label>
                            <input class="form-control date" name="birth_date" placeholder="YYYY-MM-DD" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-end-date="0d" value="{{ old('birth_date', $runner->birth_date) }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="gender">Género</label><br>
                            <select class="custom-select" name="gender">
                                <option value="">Seleccione un Género</option>
                                <option value="0" {{ old('gender', $runner->gender) == "0" ? 'selected' : '' }}>Masculino</option>
                                <option value="1" {{ old('gender', $runner->gender) == "1" ? 'selected' : '' }}>Femenino</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="authorization" style="display: none;">
                    <hr>
                    <h4>Información del Padre, Madre o Encargado</h4>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="relative_name">Nombre</label>
                                <input class="form-control" name="relative_name" aria-describedby="relativeNameHelp" value="{{ old('relative_name', $runner->relative_name) }}">
                                <small id="relativeNameHelp" class="form-text text-muted">Nombre del padre, madre o encargado.</small>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="relative_email">Correo Electrónico</label>
                                <input class="form-control" name="relative_email" aria-describedby="relativeEmailHelp" value="{{ old('relative_email', $runner->relative_email) }}">
                                <small id="relativeEmailHelp" class="form-text text-muted">Correo Electrónico del padre, madre o encargado.</small>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="relative_phone">Teléfono</label>
                                <input type="tel" class="form-control phone" name="relative_phone" aria-describedby="relativePhoneHelp" placeholder="(999) 999-9999" value="{{ old('relative_phone', $runner->relative_phone) }}">
                                <small id="relativePhoneHelp" class="form-text text-muted">Teléfono del padre, madre o encargado.</small>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="street_1">Dirección 1</label>
                            <input class="form-control" name="street_1" aria-describedby="street1Help" value="{{ old('street_1', $runner->street_1) }}">
                            <small id="street1Help" class="form-text text-muted">Escriba la dirección completa en la cual recibe su correspondencia.</small>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="street_1">Dirección 2</label>
                            <input class="form-control" name="street_2" aria-describedby="street2Help" value="{{ old('street_2', $runner->street_2) }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="city_id">Pueblo</label><br>
                            <select class="custom-select city" name="city_id" onchange="loadZips(this)">
                                <option value="">Seleccione el Pueblo de Residencia</option>
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" {{ old('city_id', $runner->city_id) == $city->id ? 'selected' : '' }}>{{ $city->description }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="country">País</label>
                            <input class="form-control" name="country" value="{{ old('country', $runner->country) }}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="zipcode_id">Zipcode</label><br>
                            <select id="zipcode_id" name="zipcode_id" class="custom-select">
                                <option value="{{ $runner->zipcode_id }}">{{ $runner->zipcode->description }}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="phone">Teléfono</label>
                            <input type="tel" class="form-control phone" name="phone" aria-describedby="phoneHelp" placeholder="(999) 999-9999" value="{{ old('phone', $runner->phone) }}">
                            <small id="phoneHelp" class="form-text text-muted">Escriba su número de teléfono.</small>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="email">Correo Electrónico</label>
                            <input type="email" class="form-control" name="email" aria-describedby="emailHelp" value="{{ old('email', $runner->email) }}">
                            <small id="emailHelp" class="form-text text-muted">Escriba su correo electrónico.</small>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12">
                        <label for="group_name">Grupo</label>
                        <input class="form-control form-control-sm" name="group_name" aria-describedby="groupNameHelp" value="{{ old('group_name', $runner->group_name) }}" placeholder="Opcional">
                        <small id="groupNameHelp" class="form-text text-muted">¿Vas a participar junto a un grupo? ¡Indícanos el nombre del mismo!</small>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="emergency_contact">Contacto de Emergencia</label>
                            <input class="form-control" name="emergency_contact" aria-describedby="emergencyContactHelp" value="{{ old('emergency_contact', $runner->emergency_contact) }}">
                            <small id="emergencyContactHelp" class="form-text text-muted">Nombre de la persona con la que nos comunicaremos en caso de una emergencia.</small>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="emergency_contact_phone">Teléfono Contacto de Emergencia</label>
                            <input type="tel" class="form-control phone" name="emergency_contact_phone" aria-describedby="emergencyContactPhoneHelp" placeholder="(999) 999-9999" value="{{ old('emergency_contact_phone', $runner->emergency_contact_phone) }}">
                            <small id="emergencyContactPhoneHelp" class="form-text text-muted">Télefono de la persona contacto en caso de una emergencia.</small>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="payment_method">Método de pago</label><br>
                            <select class="custom-select" name="payment_method">
                                <option value="">Seleccione un método de pago</option>
                                <option value="ATH Móvil" @if($runner->payment_method == 'ATH Móvil') selected @endif>ATH Móvil</option>
                                <option value="PayPal" @if($runner->payment_method == 'PayPal') selected @endif>PayPal</option>
                                <option value="Cash" @if($runner->payment_method == 'Cash') selected @endif>Cash</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <p>Tamaño de Camisa:</p>
                    </div>
                    <div class="col-sm-10">
                        @foreach($shirts as $shirt)
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="shirt_id" id="{{ $shirt->id }}" value="{{ $shirt->id }}" {{ (collect(old('shirt_id', $runner->shirt_id))->contains($shirt->id)) ? 'checked':'' }}> {{ $shirt->size }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>

                <button class="btn btn-primary">Actualizar</button>
            </form>

            <!-- Menor de 18 años -->
            <div class="modal fade" id="young_registration" tabindex="-1" role="dialog" aria-labelledby="young_registration" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Inscripción menores de 18 años</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            La inscripción para un menor de 18 años debe estar autorizada por el padre, madre o encargado
                            del joven. Favor llenar la información de contacto requerida a continuación.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button class="btn btn-primary" onclick="$('#young_registration').modal('hide');">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/waitingfor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/zipcode_picker.js') }}"></script>

    <script type="text/javascript">

        $('.phone').mask('?(999) 999-9999', {placeholder:"(###) ###-####"});
        $('.date').mask('9999-99-99', {placeholder:"YYYY-MM-DD"});

        $(".date").change(function(){

            var today = new Date();
            var birthDate = new Date($('.date').val());
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }

            if (age < 18) {
                $('#young_registration').modal('show')
                $('#authorization').show()
            } else {
                $('#authorization').hide()
            }

            return $('#age').html(age+' years old');
        });

    </script>

@endsection
