@extends('admin.layout.main')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Lista de Corredores ({{ \App\Runner::count() }})</h3>
                    </div>
                    <div class="col-sm-12">
                        <a href="{{ route('runners.create') }}" class="btn btn-primary">Inscribir Corredor</a>
                        <a href="{{ route('runners.export') }}" class="btn btn-success">Descargar CSV</a>
                    </div>
                </div>
            </div>

            <hr>

            <table id="runners" class="table table-responsive-lg table-hover">
                <thead>
                <tr>
                    <th>Bib</th>
                    <th>Nombre</th>
                    <th>T-Shirt</th>
                    <th>Sexo</th>
                    <th>Carrera</th>
                    <th>Método de Pago</th>
                    <th>Emergencia</th>
                </tr>
                </thead>
                <tbody>
                @foreach($runners as $runner)

                    <tr class="{{ $runner->confirmed == 0 ? "table-danger" : "table-success" }}">
                        <td>{{ $runner->id }}</td>
                        <td><a href="{{ route('runners.edit', $runner) }}">{{ $runner->full_name }}</a></td>
                        <td>{{ $runner->shirt()->first()->size }}</td>
                        <td>{{ $runner->gender == "0" ? 'Masculino' : 'Femenino' }}</td>
                        <td>{{ $runner->race->description }}</td>
                        <td>{{ $runner->payment_method }}</td>
                        <td>{{ $runner->emergency_contact }} <br> {{ $runner->emergency_contact_phone }}</td>
                    </tr>

                @endforeach

                </tbody>
            </table>
        </div>

    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('#runners').DataTable({
                responsive: true,
            });
        } );
    </script>

@endsection
