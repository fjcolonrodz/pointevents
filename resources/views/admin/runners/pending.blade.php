@extends('admin.layout.main')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>{{ $event->name }}- Corredores por Confirmar ({{ $runners->count() }})</h2>
                    </div>

                    <div class="col-sm-12">
{{--                        <a href="{{ route('runners.printUnconfirmed') }}" class="btn btn-danger">Imprimir Atletas Sin Confirmar</a>--}}
                    </div>
                </div>
            </div>

            <hr>

            <table id="runners" class="table table-responsive-lg table-hover">
                <thead>
                <tr>
                    <th>Bib</th>
                    <th>Nombre</th>
                    <th>T-Shirt</th>
                    <th>Sexo</th>
                    <th>Carrera</th>
                    <th>Código</th>
                    <th>Emergencia</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($runners as $runner)

                    <tr class="{{ $runner->confirmed == 0 ? "table-danger" : "table-success" }}">
                        <td>{{ $runner->id }}</td>
                        <td>{{ $runner->full_name }}</td>
                        <td>{{ $runner->shirt()->first()->size }}</td>
                        <td>{{ $runner->gender == "0" ? 'Masculino' : 'Femenino' }}</td>
                        <td>{{ $runner->race->description }}</td>
                        <td>{{ $runner->confirmation_code }}</td>
                        <td>{{ $runner->emergency_contact }} <br> {{ $runner->emergency_contact_phone }}</td>
                        <td>
                            @if($runner->confirmed == 0)
                                <a href="{{ route('events.runners.confirm', ['event' => $event, 'runner' => $runner]) }}" class="btn btn-sm btn-danger">Confirmar Inscripción</a>
                            @else
                                <a href="{{ $runner->id }}/unconfirm" class="btn btn-sm btn-success">Confirmado</a>
                            @endif
                        </td>
                    </tr>

                @endforeach

                </tbody>
            </table>
        </div>

    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('#runners').DataTable({
                responsive: true,
            });
        } );
    </script>

@endsection