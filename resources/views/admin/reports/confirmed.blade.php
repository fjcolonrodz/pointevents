<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8" />
{{--    <title>{{ $reportTitle }}</title>--}}
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
    <style>
        html {
            margin: 15.3em 1.5em 5em 1.5em;
        }

        body {
            padding: 0;
            background-color: white;
            font-family: 'Open Sans', sans-serif;
            font-size: 12px;
            overflow-x: hidden;
            width: 100%;
        }
        header {
            position: fixed;
            top: -18em;
        }
        table{
            margin-top: -8em;
            width: 100%;
        }
        th{
            background-color: orangered;
            color: white;
            vertical-align: middle;
        }
        .center {
            text-align: center;
        }
        .disclaimer {
            font-size: 9px;
            position: fixed;
            bottom: -4em;
        }
        .footer {
            font-size: 9px;
            position: fixed;
            left: 950px;
            bottom: -4em;
        }
        .generatedDate {
            font-size: 9px;
            position: fixed;
            top: -22.5em;
            left: 621px;
        }
        .headerText {
            text-align: center;
        }
        .logo{
            position: absolute;
        }
        .page-break {
            page-break-after: always;
        }
        .pagenum:before {
            content: counter(page);
        }
    </style>
</head>
<body>
<header>
    <div class="logo">
        <img src="{{ asset('img/logo.png') }}" alt="Logo Extensión Agrícola" width="100">
    </div>

    <div class="headerText">
        <h3>
            Academia de la Inmaculada Concepción <br>
            Carrera 5Knights <br>
            Tercera Edición <br>
            {{ $report_title }}
        </h3>
    </div>
    <hr>
</header>

<table class="pure-table pure-table-bordered">
    <thead>
    <tr>
        <th>Nombre Completo</th>
        <th>Evento</th>
        <th>BIB</th>
        <th>Teléfono</th>
        <th>Edad</th>
        <th>Sexo</th>
        <th>Tamaño de Camisa</th>
    </tr>
    </thead>
    <tbody>
        @foreach($runners as $runner)
            <tr>
                <td>{{ $runner->full_name }}</td>
                <td>{{ $runner->race_type()->first()->description }}</td>
                <td>{{ $runner->bib_number }}</td>
                <td>{{ $runner->phone }}</td>
                <td>{{ $runner->birth_date }} años</td>
                <td>{{ $runner->gender == "0" ? 'Masculino' : 'Femenino' }}</td>
                <td>{{ $runner->shirt()->first()->size }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="footer">Página: <span class="pagenum"></span></div>

</body>
</html>