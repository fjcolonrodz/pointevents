@extends('admin.layout.main')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h3>Próximos Eventos</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            @foreach($events as $event)
                <div class="card text-white bg-primary mb-3">
                    <div class="card-header text-center">{{ $event->name }}</div>
                    <div class="card-body text-center">
                        <h2 class="card-title">{{ $event->runners->count() }}</h2>
                        <p class="card-text">atletas registrados</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    {{--<div class="row mt-4">--}}
        {{--<div class="col-sm-12">--}}
            {{--<h3>Camisas disponibles</h3>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
        {{--@foreach($shirts as $shirt)--}}
            {{--<div class="col-sm-2">--}}
                {{--<div class="card text-white bg-dark mb-3">--}}
                    {{--<div class="card-header text-center">Tamaño: {{ $shirt->size }}</div>--}}
                    {{--<div class="card-body text-center">--}}
                        {{--<h2 class="card-title">{{ $shirt->quantity - \App\Runner::where('shirt_id', $shirt->id)->count() }}</h2>--}}
                        {{--<p class="card-text">disponibles</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endforeach--}}
    {{--</div>--}}

@endsection