@extends('admin.layout.main')

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Eventos Registrados ({{ $events->count() }})</h3>
                    </div>
                </div>
            </div>

            <hr>

            <table id="runners" class="table table-responsive-lg table-hover">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Coordinador</th>
                    <th>E-mail</th>
                    <th>Fecha</th>
                    <th>Precio</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($events as $event)

                        <tr class="table-success">
                            <td>{{ $event->name }}</td>
                            <td>{{ $event->user->name }}</td>
                            <td>{{ $event->email }}</td>
                            <td>{{ $event->date->format('M d, Y') }}</td>
                            <td>${{ $event->price }}.00</td>
                            <td><a href="{{ route('events.runners.index', $event) }}" class="btn btn-sm btn-primary">Corredores</a></td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('#runners').DataTable({
                responsive: true,
            });
        } );
    </script>

@endsection