@extends('admin.layout.main')

@section('content')
    <div class="row mt-2">
        <div class="col-sm-12">
            <h3>Camisas disponibles</h3>
        </div>
    </div>
    <div class="row">
        @foreach($event->shirts as $shirt)
            <div class="col-md-2">
                <div class="card text-white bg-dark mb-3">
                    <div class="card-header text-center">Tamaño: <br>{{ $shirt->size }}</div>
                    <div class="card-body text-center">
                        <h2 class="card-title">{{ $shirt->quantity - $event->runners->where('shirt_id', $shirt->id)->count() }}</h2>
                        <p class="card-text">disponibles</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>{{ $event->name }} - Lista de Corredores ({{ $event->runners->count() }})</h3>
                            </div>
                            <div class="col-sm-12">
                                <a href="{{ route('events.runners.create', $event) }}" class="btn btn-primary">Inscribir Corredor</a>
{{--                                <a href="{{ route('events.runners.pending', $event) }}" class="btn btn-danger">Pendientes</a>--}}
                                <a href="{{ route('events.runners.export', $event) }}" class="btn btn-success">Descargar CSV</a>
                                <a href="{{ route('events.runners.export_all', $event) }}" class="btn btn-success">Descargar Corredores</a>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <table id="runners" class="table table-responsive-lg table-hover">
                        <thead>
                        <tr>
                            <th>Bib</th>
                            <th>Nombre</th>
                            <th>T-Shirt</th>
                            <th>Sexo</th>
                            <th>Carrera</th>
                            <th>Método de Pago</th>
                            <th>Emergencia</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($event->runners as $runner)

                            <tr class="{{ $runner->confirmed == 0 ? "table-danger" : "table-success" }}">
                                <td>{{ $runner->id }}</td>
                                <td><a href="{{ route('events.runners.edit', ['event' => $event, 'runner' => $runner]) }}">{{ $runner->full_name }}</a></td>
                                <td>{{ $runner->shirt()->first()->size }}</td>
                                <td>{{ $runner->gender == "0" ? 'Masculino' : 'Femenino' }}</td>
                                <td>{{ $runner->race->description }}</td>
                                <td>{{ $runner->payment_method }}</td>
                                <td>{{ $runner->emergency_contact }} <br> {{ $runner->emergency_contact_phone }}</td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('#runners').DataTable({
                responsive: true,
            });
        } );
    </script>

@endsection
