<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Colorlib">
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <!-- Page Title -->
    <title>5k Candel Coop</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet">
    <!-- Simple line Icon -->
    <link rel="stylesheet" href="{{ asset('css/simple-line-icons.css') }}">
    <!-- Themify Icon -->
    <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}">
    <!-- Hover Effects -->
    <link rel="stylesheet" href="{{ asset('css/set1.css') }}">
    <!-- Swipper Slider -->
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <!-- Datepicker CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>
<!--============================= HEADER =============================-->
<div class="dark-bg sticky-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light">
{{--                    <img src="{{ asset('images/pe_logo.png') }}" class="navbar-brand" alt="" height="70">--}}
                    <a class="navbar-brand" href="{{ route('events.show', 1) }}">5k Candel Coop</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-menu"></span>
                    </button>
                    {{--<div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">--}}
                        {{--<ul class="navbar-nav">--}}
                            {{--<li class="nav-item dropdown">--}}
                                {{--<a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                    {{--Explore--}}
                                    {{--<span class="icon-arrow-down"></span>--}}
                                {{--</a>--}}
                                {{--<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">--}}
                                    {{--<a class="dropdown-item" href="#">Action</a>--}}
                                    {{--<a class="dropdown-item" href="#">Another action</a>--}}
                                    {{--<a class="dropdown-item" href="#">Something else here</a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item dropdown">--}}
                                {{--<a class="nav-link" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                    {{--Listing--}}
                                    {{--<span class="icon-arrow-down"></span>--}}
                                {{--</a>--}}
                                {{--<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">--}}
                                    {{--<a class="dropdown-item" href="#">Action</a>--}}
                                    {{--<a class="dropdown-item" href="#">Another action</a>--}}
                                    {{--<a class="dropdown-item" href="#">Something else here</a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item dropdown">--}}
                                {{--<a class="nav-link" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                    {{--Pages--}}
                                    {{--<span class="icon-arrow-down"></span>--}}
                                {{--</a>--}}
                                {{--<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">--}}
                                    {{--<a class="dropdown-item" href="#">Action</a>--}}
                                    {{--<a class="dropdown-item" href="#">Another action</a>--}}
                                    {{--<a class="dropdown-item" href="#">Something else here</a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item active">--}}
                                {{--<a class="nav-link" href="#">About</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="#">Blog</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#" class="btn btn-outline-light top-btn"><span class="ti-plus"></span> Add Listing</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </nav>
            </div>
        </div>
    </div>
</div>

@yield('content')

<!--============================= FOOTER =============================-->
<footer class="main-block dark-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <p>Copyright &copy; 2020 <a href="https://kmilemedia.com">Kmile Media Designs</a>. All rights reserved</p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <ul>
                        <li><a href="https://fb.me/e/31sY61gTu"><span class="ti-facebook"></span></a></li>
                        {{--<li><a href="#"><span class="ti-twitter-alt"></span></a></li>--}}
                        {{--<li><a href="#"><span class="ti-instagram"></span></a></li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--//END FOOTER -->

@yield('scripts')

</body>

</html>
