@extends('public.layout')

@section('content')

    <!--============================= BOOKING =============================-->
    <div>
        <!-- Swiper -->
        <div class="swiper-container">
            <div class="swiper-wrapper">

                <div class="swiper-slide">
                    <a href="{{ asset('images/slideshow/1.jpg') }}" class="grid image-link">
                        <img src="{{ asset('images/slideshow/1.jpg') }}" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{ asset('images/slideshow/7.jpg') }}" class="grid image-link">
                        <img src="{{ asset('images/slideshow/7.jpg') }}" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{ asset('images/slideshow/2.jpg') }}" class="grid image-link">
                        <img src="{{ asset('images/slideshow/2.jpg') }}" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{ asset('images/slideshow/3.jpg') }}" class="grid image-link">
                        <img src="{{ asset('images/slideshow/3.jpg') }}" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{ asset('images/slideshow/4.jpg') }}" class="grid image-link">
                        <img src="{{ asset('images/slideshow/4.jpg') }}" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{ asset('images/slideshow/5.jpg') }}" class="grid image-link">
                        <img src="{{ asset('images/slideshow/5.jpg') }}" class="img-fluid" alt="#">
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{ asset('images/slideshow/6.jpg') }}" class="grid image-link">
                        <img src="{{ asset('images/slideshow/6.jpg') }}" class="img-fluid" alt="#">
                    </a>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-white"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
        </div>
    </div>
    <!--//END BOOKING -->
    <!--============================= RESERVE A SEAT =============================-->
    <section class="reserve-block">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h5>{{ $event->name }}</h5> <br>
                    <p class="reserve-description">27 al 30 de enero de 2021</p>
                </div>
                <div class="col-md-6">
                    <div class="reserve-seat-block">
                        <div class="reserve-btn">
                            <div class="featured-btn-wrap">
                                <a href="" class="btn btn-danger" data-toggle="modal" data-target="#register">Inscripciones Cerradas</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//END RESERVE A SEAT -->
    <!--============================= BOOKING DETAILS =============================-->
    <section class="light-bg booking-details_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-8 responsive-wrap">
                    <div class="booking-checkbox_wrap">
                        <div class="booking-checkbox">
                            {!! html_entity_decode($event->description) !!}
                            <hr>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{ asset('images/logos/logo_candel.jpg') }}" alt="" height="100">
                                <img src="{{ asset('images/logos/logo_asa.png') }}" alt="" height="100">
                                <img src="{{ asset('images/logos/manati_auto.png') }}" alt="" height="100">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 responsive-wrap">
                    <div class="contact-info">
                        <img src="{{ asset($event->logo) }}" class="img-fluid" alt="#">
                        <div class="address">
                            <span class="icon-screen-smartphone"></span>
                            <p>{{ $event->phone }}</p>
                        </div>
                        <div class="address">
                            <span class="icon-envelope"></span>
                            <p>{{ $event->email }}</p>
                        </div>
                        <div class="address">
                            <span class="icon-link"></span>
                            <p>{{ $event->website }}</p>
                        </div>
                        <div class="address">
                            <span class="icon-clock"></span>
                            <p>27 al 30 de enero de 2021<br>
                        </div>
                        <a href="#" class="btn btn-outline-danger btn-contact" data-toggle="modal" data-target="#register">INSCRIPCIONES CERRADAS</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//END BOOKING DETAILS -->

{{--    @include('public.modals.register_new')--}}
    @include('public.modals.register_close')

    <!-- Modal: Relevo de Responsabilidad -->
    @include('public.modals.agreement')
    @include('public.modals.payment')

@endsection

@section('scripts')

    <!-- jQuery, Bootstrap JS. -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Magnific popup JS -->
    <script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
    <!-- Swipper Slider JS -->
    <script src="{{ asset('js/swiper.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.maskedinput.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/waitingfor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/zipcode_picker.js') }}"></script>
    @include('js.masks')
    @include('js.ageVerification')
    @include('js.paypal')
    @include('js.validateForm')

    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 3,
            slidesPerGroup: 3,
            loop: true,
            loopFillGroupWithBlank: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script>
    <script>
        if ($('.image-link').length) {
            $('.image-link').magnificPopup({
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        }
        if ($('.image-link2').length) {
            $('.image-link2').magnificPopup({
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        }
    </script>


@endsection
