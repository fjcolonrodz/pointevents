<div class="modal fade" id="terms_acceptance" tabindex="-1" role="dialog" aria-labelledby="terms_acceptance" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Relevo de Responsabilidad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-justify">
                ESTOY DE ACUERDO EN QUE TODA DECLARACIÓN, RENUNCIA DE DERECHOS, RELEVO, CONVENIO, CONSENTIMIENTO Y
                PERMISO QUE YO OTORGUE POR MEDIO DE LA PRESENTE ES OTORGADO A MI NOMBRE Y A NOMBRE DE TODOS MIS HIJOS
                MENORES DE EDAD O PERSONAS DE LAS QUE YO SOY TUTOR LEGAL Y QUE PARTICIPEN O ESTÉN PRESENTES EN EL EVENTO.
                Doy mi consentimiento y autorización a "ARRANCA PAL' 5K DE CANDEL COOP", CANDEL COOP & ASA PRODUCTION, INC.
                sus afiliadas y carreras, sus patrocinadores, incluyendo patrocinadores corporativos, sus sucesores,
                licenciatarios, y cesionarios el derecho irrevocable a utilizar, para cualquier propósito y sin
                compensación, (i) toda fotografía, videograbación, grabación de audio y demás grabaciones que se hagan
                de mi persona durante el curso de "ARRANCA PAL' 5K DE CANDEL COOP" y (ii) los resultados de mi
                participación en "ARRANCA PAL' 5K DE CANDEL COOP" (por ejemplo la hora de la carrera, mi nombre y mi
                número de participante). Entiendo que (i) al dar mi consentimiento a estas disposiciones lo hago en
                consideración por permitírseme participar en "ARRANCA PAL' 5K DE CANDEL COOP"; (ii) puedo ser removido
                de esta competencia si no obedezco las reglas de este Evento; y (iii) participo voluntariamente en
                "ARRANCA PAL' 5K DE CANDEL COOP". Mi estado físico y de salud es bueno y yo soy la única persona
                responsable por mi salud personal, mi seguridad, y de los bienes que son de mi propiedad. Reconozco que
                este evento es una actividad potencialmente peligrosa y por medio de la presente asumo total responsabilidad
                por cualquier lesión o accidente que pueda ocurrir durante el transcurso de mi participación en
                "ARRANCA PAL' 5K DE CANDEL COOP" (INCLUYENDO, SIN LIMITAR, MIS ACTIVIDADES DE RECOPILACIÓN DE FONDOS
                asociadas con el evento) o mientras ME ENCUENTRE EN LAS INSTALACIONES DEL EVENTO (COLECTIVAMENTE, “MI PARTICIPACIÓN”).
                ACTUANDO A MI PROPIO NOMBRE ASÍ COMO A NOMBRE DE MIS SUCESORES, FAMILIARES, ADMINISTRADORES, Y ALBACEAS
                (COLECTIVAMENTE LOS “RENUNCIANTES”), POR ESTE MEDIO LIBERO DE LA FORMA MAS AMPLIA QUE PERMITA LA LEY,
                EXONERO DE RESPONSABILIDAD Y ME COMPROMETO A NO ENTABLAR ACCIÓN LEGAL ALGUNA CONTRA "ARRANCA PAL' 5K DE CANDEL COOP",
                CANDEL COOP & ASA PRODUCTION, INC. sus afiliadas y carreras, sus patrocinadores, incluyendo patrocinadores
                corporativos, sus sucesores, licenciatarios, cesionarios, sus directores, funcionarios, voluntarios,
                agentes y empleados respectivos; (II) LOS PROMOTORES DEL EVENTO; Y (III) TODA PERSONA NATURAL O JURÍDICA
                ASOCIADA CON ESTE EVENTO (LOS "EXONERADOS"), DE CUALQUIER PERDIDA, RESPONSABILIDAD O RECLAMO QUE PUEDA
                SURGIR A RAÍZ DE MI PARTICIPACIÓN EN ESTE EVENTO. ESTA RENUNCIA DE DERECHOS O DESCARGO APLICA A TODA
                PERDIDA, RESPONSABILIDAD O RECLAMO QUE PUDIERA TENER YO EN LO PERSONAL O MIS RENUNCIANTES EN RELACIÓN
                CON MI PARTICIPACIÓN, INCLUYENDO LESIÓN PERSONAL O DAÑOS SUFRIDOS POR MI O POR OTROS, YA SEA QUE ESTOS
                SEAN CAUSADOS POR CAÍDAS, CONTACTO CON Y/O LAS ACCIONES DE OTROS PARTICIPANTES, CONTACTO CON OBJETOS
                FIJOS O NO FIJOS, CONTACTO CON ANIMALES, CONDICIONES DE LAS INSTALACIONES DEL EVENTO, NEGLIGENCIA DE
                LOS EXONERADOS, RIESGOS NO CONOCIDOS POR MI O QUE NO SON RAZONABLEMENTE ANTICIPABLES EN ESTE MOMENTO,
                O CUALQUIER OTRA CAUSA. COMPRENDO QUE SOY LA ÚNICA PERSONA RESPONSABLE POR TODOS LOS ASPECTOS DE MIS
                ACTIVIDADES DE RECOLECCIÓN DE FONDOS ASOCIADA CON MI PARTICIPACIÓN, INCLUYENDO SIN LIMITAR EL QUE SE
                CONDUZCAN DE MANERA SEGURA Y LEGAL DICHAS ACTIVIDADES DE RECOLECCIÓN DE FONDOS. Esta Renuncia y Relevo
                de Responsabilidad Respecto a los Resultados y Fotografías y Renuncia y Relevo de Responsabilidad
                Respecto a Reclamaciones (colectivamente la “Renuncia”) deberá interpretarse conforme a las leyes del
                estado en el que tenga lugar el Evento. En caso de que cualquier disposición de esta Renuncia sea
                considerada inválida conforme a la ley aplicable, (i) Candel Coop, ASA Production, Inc tendrán el
                derecho de modificar dicha disposición en la medida que sea necesario para ser válida; y (ii) todas las
                demás disposiciones de esta Renuncia continuarán en pleno vigor. Entiendo que he renunciado a derechos
                importantes mediante la firma de esta Renuncia, y he firmado esta Renuncia de forma libre y voluntaria,
                sin que se me haya inducido a hacerlo, y sin que se me asegure ni garantice nada respecto a la misma.
                Mi intención es que mi firma constituya una renuncia completa e incondicional de responsabilidad en la
                medida más amplia que permita la ley.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#payment" data-dismiss="modal">Aceptar y Pagar</button>


                {{--<button class="btn btn-primary" data-dismiss="modal" onclick="showPayment()">Aceptar</button>--}}
            </div>
        </div>
    </div>
</div>