<div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="payment" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pago de inscripción</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div id="adult" class="col-sm-12 position-fixed">
                    <div class="card">
                        <div class="card-header">
                        Realizar Pago
                        </div>
                        <div class="card-body">
                            <h4>Total a pagar</h4>
                            <br>
                            <div class="row">

                                <div class="col-sm-4">
                                    <strong>Total:</strong>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <strong>$ 22.00</strong>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div id="paypal-button"></div>
                                    <hr>
                                    <div id="ATHMovil_Checkout_Button"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="child" class="col-sm-12 position-fixed">
                    <div class="card">
                        <div class="card-header">
                            Realizar Pago
                        </div>
                        <div class="card-body">
                            <h4>Total a pagar</h4>
                            <br>
                            <div class="row">

                                <div class="col-sm-4">
                                    <strong>Total:</strong>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <strong>$ 0.00</strong>
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn btn-danger btn-block" onclick="processKidRegistration()">Inscribir</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
