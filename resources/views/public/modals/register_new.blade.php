<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <form id="registration_form">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Inscripción Electrónica</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="race_id">Carrera</label>
                                <select id="race_id" name="race_id" class="form-control form-control-sm" required>
                                    <option value="">Seleccione el Tipo de Carrera</option>
                                    @foreach($event->races as $race)
                                        <option value="{{ $race->id }}" {{ old('race_id') == $race->id ? 'selected' : '' }}>{{ $race->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-5 col-lg-4">
                                <label for="name">Nombre</label>
                                <input id="name" name="name" class="form-control form-control-sm" value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group col-sm-12 col-md-2 col-lg-1">
                                <label for="initial">Inicial</label>
                                <input id="initial" name="initial" class="form-control form-control-sm" value="{{ old('initial') }}">
                            </div>
                            <div class="form-group col-sm-12 col-md-5 col-lg-4">
                                <label for="last_name">Apellidos</label>
                                <input id="last_name" name="last_name" class="form-control form-control-sm" value="{{ old('last_name') }}" required>
                            </div>
                            <div class="form-group col-sm-12 col-md-12 col-lg-3">
                                <label for="birth_date">Fecha de Nacimiento</label>
                                <input id="birth_date" name="birth_date" class="form-control form-control-sm date" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-end-date="0d" value="{{ old('birth_date') }}" required>
                            </div>
                        </div>

                        <div id="authorization" style="display:none;">
                            <hr>
                            <h5 class="text-center">Información del Padre, Madre o Encargado</h5>
                            <p style="text-align: center;">La inscripción para un menor de 18 años debe estar autorizada por el padre, madre o encargado
                                del joven.</p>
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label for="relative_name">Nombre</label>
                                    <input class="form-control form-control-sm" name="relative_name" aria-describedby="relativeNameHelp" value="{{ old('relative_name') }}">
                                    <small id="relativeNameHelp" class="form-text text-muted">Nombre del padre, madre o encargado.</small>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="relative_email">Correo Electrónico</label>
                                    <input class="form-control form-control-sm" name="relative_email" aria-describedby="relativeEmailHelp" value="{{ old('relative_email') }}">
                                    <small id="relativeEmailHelp" class="form-text text-muted">Correo Electrónico del padre, madre o encargado.</small>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="relative_phone">Teléfono</label>
                                    <input type="tel" class="form-control form-control-sm phone" name="relative_phone" aria-describedby="relativePhoneHelp" placeholder="(999) 999-9999" value="{{ old('relative_phone') }}">
                                    <small id="relativePhoneHelp" class="form-text text-muted">Teléfono del padre, madre o encargado.</small>
                                </div>
                            </div>
                            <hr>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="street_1">Dirección Postal</label>
                                <input id="street_1" name="street_1" class="form-control form-control-sm" aria-describedby="street1Help" value="{{ old('address_1') }}" required>
                                <small id="street1Help" class="form-text text-muted">Escriba la dirección completa en la cual recibe su correspondencia.</small>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="street_2" style="display: none"></label>
                                <input id="street_2" name="street_2" class="form-control form-control-sm" value="{{ old('address_2') }}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-4">
                                <label for="city_id">Pueblo</label><br>
                                <select class="form-control form-control-sm" name="city_id" onchange="loadZips(this)" required>
                                    <option value="">Seleccione un pueblo</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-4">
                                <label for="country">País</label>
                                <input id="country" name="country" class="form-control form-control-sm" value="{{ old('country', 'Puerto Rico') }}" required>
                            </div>
                            <div class="form-group col-sm-12 col-md-4">
                                <label for="zipcode_id">Zipcode</label><br>
                                <select id="zipcode_id" name="zipcode_id" class="custom-select form-control form-control-sm" required>
                                    <option value="">Selecciona código postal</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-4">
                                <label for="gender">Género</label><br>
                                <select class="custom-select form-control form-control-sm" name="gender" required>
                                    <option value="">Seleccione un género</option>
                                    <option value="0" {{ old('gender') == "0" ? 'selected' : '' }}>Masculino</option>
                                    <option value="1" {{ old('gender') == "1" ? 'selected' : '' }}>Femenino</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-4">
                                <label for="phone">Teléfono</label>
                                <input name="phone" type="tel" class="form-control form-control-sm phone" value="{{ old('phone') }}" requiredx>
                            </div>
                            <div class="form-group col-sm-12 col-md-4">
                                <label for="email">Correo Electrónico</label>
                                <input name="email" type="email" class="form-control form-control-sm" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="group_name">Grupo</label>
                                <input class="form-control form-control-sm" name="group_name" aria-describedby="groupNameHelp" value="{{ old('group_name') }}" placeholder="Opcional">
                                <small id="groupNameHelp" class="form-text text-muted">¿Vas a participar junto a un grupo? ¡Indícanos el nombre del mismo!</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12 col-md-6">
                                <label for="emergency_contact">Contacto de Emergencia</label>
                                <input class="form-control form-control-sm" name="emergency_contact" aria-describedby="emergencyContactHelp" value="{{ old('emergency_contact') }}" required>
                                <small id="emergencyContactHelp" class="form-text text-muted">Nombre de la persona contacto en caso de una emergencia.</small>
                            </div>
                            <div class="form-group col-sm-12 col-md-6">
                                <label for="emergency_contact_phone">Teléfono de Emergencia</label>
                                <input type="tel" class="form-control form-control-sm phone" name="emergency_contact_phone" aria-describedby="emergencyContactPhoneHelp" value="{{ old('emergency_contact_phone') }}" required>
                                <small id="emergencyContactPhoneHelp" class="form-text text-muted">Télefono de la persona contacto en caso de una emergencia.</small>
                            </div>
                        </div>

                        <div id="shirt_id" class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="shirt_id">Tamaño de Camisa</label><br>
                                <select class="form-control form-control-sm" name="shirt_id" required>
                                    <option value="">Seleccione un tamaño de camisa</option>
                                    @foreach($event->shirts as $shirt)
                                        <option value="{{ $shirt->id }}" {{ (collect(old('shirt_id'))->contains($shirt->id)) ? 'selected':'' }}>{{ $shirt->size }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="validateForm()">Inscribirme</button>
                </div>
                @csrf
            </form>
        </div>
    </div>
</div>
