<div class="modal fade" id="young_registration" tabindex="-1" role="dialog" aria-labelledby="young_registration" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Inscripción menores de 18 años</h5>
            </div>
            <div class="modal-body text-justify">
                <p>
                    La inscripción para un menor de 18 años debe estar autorizada por el padre, madre o encargado
                    del joven.
                </p>
                 <p>Favor llenar la información de contacto requerida.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" onclick="$('#young_registration').modal('hide');">Aceptar</button>
            </div>
        </div>
    </div>
</div>